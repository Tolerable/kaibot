﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;
using RestSharp.Extensions.MonoHttp;
using RestSharp;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Linq;

namespace KaiBot
{
	internal enum AccentState {
		ACCENT_DISABLED = 0,
		ACCENT_ENABLE_GRADIENT = 1,
		ACCENT_ENABLE_TRANSPARENTGRADIENT = 2,
		ACCENT_ENABLE_BLURBEHIND = 3,
		ACCENT_INVALID_STATE = 4
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct AccentPolicy {
		public AccentState AccentState;
		public int AccentFlags;
		public int GradientColor;
		public int AnimationId;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WindowCompositionAttributeData {
		public WindowCompositionAttribute Attribute;
		public IntPtr Data;
		public int SizeOfData;
	}

	internal enum WindowCompositionAttribute {
		WCA_ACCENT_POLICY = 19
	}

    struct MessageFormatted {
        public MessageFormatted(string text, Color colour) {
            string[] tokens = text.TrimEnd().Split(' ');
            Sender = tokens[0];
            Body = text.Substring(Sender.Length);
            NameColour = new SolidColorBrush(colour);
        }

        public string Sender { get; set; }
        public string Body { get; set; }

        public SolidColorBrush NameColour { get; set; }
    }

    struct User {
        public string DisplayName { get; set; }
    }

    struct Message {
        public Message(string sender, string body) {
            Sender = sender;
            Body = body;
        }

        public string Sender { get; set; }
        public string Body { get; set; }
    }

    public struct MessageToken {
        public MessageToken(string token) {
            Token = token;
            Children = new List<MessageToken>();
        }

        public MessageToken(string token, List<MessageToken> children) {
            Token = token;
            Children = children;
        }

        public override string ToString() {
            string output = Token;

            for (int i = 0; i < Children.Count; i++) {
                output += "\n" + Children[i].ToString();
            }

            return output;
        }

        public string Token { get; set; }
        public List<MessageToken> Children { get; set; }
    }

    public partial class Main : Window {
        [DllImport("user32.dll")]
        internal static extern int SetWindowCompositionAttribute(IntPtr hwnd, ref WindowCompositionAttributeData data);
        ThreadStart threadReference_tick;
        Thread thread_tick;
        static Main window;
        static Session session;
        Dictionary<string, string> tokenCommands;
        public static RestClient restClient;
        private static Dictionary<string, User> users;
        static string clientID = "n2bnl069upvv7tcm1c1elo7tz8tonf";
        static string clientSecret = "qa84f32gr3mj82ingnhsdvpnoippkd";
        static List<Command> commands;
        static Dictionary<string, string> commandTemplates;
        static EnumChannel channelStreamer;
        static string user = "kaiodenic";
        static List<string> queueMessageOut;
        static int port = 6667;
        static TcpClient tcpClient;
        static StreamReader reader;
        static StreamWriter writer;
        static string passStream = "";
        static string passBot = "oauth:4y8hybx61syq3lt5v0g0hpqos7x9xo";
        static string nameStream = "kaiodenic";
        static string nameBot = "TolerableShrimp";
        static int delayRead = 500;
        static int delayWrite = 1000;
        static bool connect;

        public Main() {
            InitializeComponent();
            DataContext = new CounterDataViewModel();
            session = new Session();
            session.Start();
            connect = true;

            tcpClient = new TcpClient();

            queueMessageOut = new List<string>();

            tokenCommands = new Dictionary<string, string>();
            tokenCommands.Add("apiRead", null);
            tokenCommands.Add("followage", null);
            tokenCommands.Add("random", null);
            tokenCommands.Add("code", null);

            users = new Dictionary<string, User>();

            commandTemplates = new Dictionary<string, string>();
            commandTemplates.Add("!followage $user", "https://api.rtainc.co/twitch/channels/$myChannel/followers/$user?format=[1]+has+been+following+$streamer-display+for+[2]");
            commandTemplates.Add("!followage", "https://api.rtainc.co/twitch/channels/$myChannel/followers/$sender?format=[1]+has+been+following+$streamer-display+for+[2]");
            commandTemplates.Add("!roll $max", "$sender rolled a $random(0,$max)");
            commandTemplates.Add("!gamble", "$sender $if($threshold(7, $random(0, 10)), \"Won\", \"Lost)\"");

            commands = new List<Command>();

            commands.Add(new Command {
                Call = "!roll $num1 $num2",
                Response = "$sender rolled a $random($num1, $num2)"
            });
            commands.Add(new Command {
                Call = "!roll $num",
                Response = "$sender rolled a $random(0, $num)"
            });
            commands.Add(new Command {
                Call = "!followage $user",
                Response = "$apiRead(https://api.rtainc.co/twitch/channels/$streamer-name/followers/$user?format=[1]+has+been+following+$streamer-display+for+[2])"
            });
            commands.Add(new Command {
                Call = "!followage",
                Response = "$apiRead(https://api.rtainc.co/twitch/channels/$streamer-name/followers/$sender?format=[1]+has+been+following+$streamer-display+for+[2])"
            });
            commands.Add(new Command {
                Call = "!time",
                Response = "$time"
            });
            commands.Add(new Command {
                Call = "!time2",
                Response = "$time(HH:mm)"
            });
            commands.Add(new Command {
                Call = "!myname",
                Response = "My name is $sender-name !"
            });
            commands.Add(new Command {
                Call = "!streamer",
                Response = "$streamer-display streams at $streamer-url !"
            });
            commands.Add(new Command {
                Call = "!so $user",
                Response = "$user-display also streams! Last time he was streaming $user-game and it was swish. Check them out at $user-url"
            });
            commands.Add(new Command {
                Call = "!check",
                Response = "$sender $if($random(1, 6) >= 4, $if($random(3, 4) >= 2, \"passed twice\", \"passed once\"), \"failed completely\") in the check"
            });

            restClient = new RestClient("https://api.twitch.tv/helix");
            restClient.AddDefaultHeader("Accept", "application/vnd.twitchtv.v3+json");
            restClient.AddDefaultHeader("Client-ID", clientID);

            Thread.Sleep(1000);
            GetFollowers("kaiodenic");

            //channelStreamer = GetChannel(user);

            queueMessageOut.Add("Hi. I am a bot 1.");
            queueMessageOut.Add("Hi. I am a bot 2.");
            queueMessageOut.Add("Hi. I am a bot 3.");
            //queueMessageOut.Add("Testing delays: Successful");
            // https://api.twitch.tv/kraken/channels/cosmowright/follows/?limit=100

            window = this;
            threadReference_tick = new ThreadStart(clockThread);
            thread_tick = new Thread(threadReference_tick);
            thread_tick.Start();
        }

        private static void Reconnect() {
            string loginstring =
                "PASS " + passBot + Environment.NewLine +
                "NICK " + nameBot + Environment.NewLine;

            tcpClient = new TcpClient("irc.chat.twitch.tv", port);
            reader = new StreamReader(tcpClient.GetStream());
            writer = new StreamWriter(tcpClient.GetStream());

            writer.WriteLine(loginstring);
            writer.WriteLine("CAP REQ :twitch.tv/membership");
            writer.WriteLine(" Join #" + nameStream);
            writer.Flush();
        }

        private static string formatMessage(string msg) {
            return $":{nameBot}!{nameBot}@{nameBot}.tmi.twitch.tv PRIVMSG #{nameStream} :{msg}";
        }

        private static void onTick() {
            if (!tcpClient.Connected) {
                Reconnect();
            }

            if (tcpClient.Available > 0 || reader.Peek() >= 0) {
                string message = reader.ReadLine();
                processMessage(message);

                if (queueMessageOut.Count > 0) {
                    Console.WriteLine(queueMessageOut[0]);
                    message = queueMessageOut[0];
                    queueMessageOut.RemoveAt(0);

                    writer.WriteLine(formatMessage(message));
                    writer.Flush();
                }
            }
        }

        private static void clockThread() {
            Thread.Sleep(1000);
            while(connect) {
                Thread.Sleep(delayRead);
                onTick();
            }
        }

        private static void processMessage(string msg) {
            string[] tokens = msg.Split(':');
            string[] header = tokens[1].Split(' ');

            // If message is to chat
            if (header[1] == "PRIVMSG") {
                string body = tokens[2];
                string sender = header[0].Split('!')[0];

                if (!users.ContainsKey(sender)) {
                    users.Add(sender, new User { DisplayName = GetChannel(sender).DisplayName });
                }

                window.addMessage(new Message { Sender = sender, Body = body }, true, Colors.Red, true, false, false);

                Message output = getMessage(sender, body.TrimEnd());
                queueMessageOut.Add(output.Body);
            }
            else if (header[1] == "PING"){
                writer.WriteLine("PONG :tmi.twitch.tv");
            }
        }

        private void addMessage(Message message, bool streamer, Color colour, bool mod, bool prime, bool turbo) {
            System.Windows.Application.Current.Dispatcher.Invoke(new Action(() => {

                MessageFormatted messageFormatted = session.getFormattedMessage(message.Sender, message.Body);
                listChat.Items.Add(messageFormatted);
            }));
        }

        private void fileWrite(string path, string output) {
            System.IO.File.WriteAllText(path, output);
        }

        private void GetFollowers(string channel) {
            string followers = "";
            string followed = "";
            string discrepancies = "";
            string pagination = "";

            for (int i = 0; i < 6; i++) {
                var client = new RestClient();
                client.BaseUrl = new Uri("https://api.twitch.tv/helix/users/");
                // /channels/:channel/follows
                var request = new RestRequest();
                request.Method = Method.GET;
                //request.Resource = "Kaiodenic" + "/subscriptions?limit=100&offset=" + (100 * i);
                request.Resource = "follows?to_id=127977639&first=100" + pagination;
                //request.Resource = "Kaiodenic" + "/followers?limit=100&offset=" + (100 * i);
                request.AddHeader("Client-ID", clientID);
                request.AddHeader("accept", "application/vnd.twitchtv.v3+json");
                request.RequestFormat = RestSharp.DataFormat.Json;

                IRestResponse response = client.Execute(request);

                JObject results = JsonConvert.DeserializeObject<JObject>(response.Content);

                Console.WriteLine(response.Content);

                pagination = "&after=" + results["pagination"]["cursor"].ToString();

                for (int e = results["data"].Count(); e > 0; e--) {
                    var users = results["data"][e-1]["from_name"];
                    followers += users + ",";
                }
            }

            pagination = "";

            {
                var client = new RestClient();
                client.BaseUrl = new Uri("https://api.twitch.tv/helix/users/");
                // /channels/:channel/follows
                var request = new RestRequest();
                request.Method = Method.GET;
                request.Resource = "follows?from_id=127977639&first=100" + pagination;
                request.AddHeader("Client-ID", clientID);
                request.AddHeader("accept", "application/vnd.twitchtv.v3+json");
                request.RequestFormat = RestSharp.DataFormat.Json;

                IRestResponse response = client.Execute(request);

                JObject results = JsonConvert.DeserializeObject<JObject>(response.Content);

                pagination = "&after=" + results["pagination"]["cursor"].ToString();

                for (int e = results["data"].Count(); e > 0; e--)
                {
                    var users = results["data"][e - 1]["to_name"];
                    followed += users + ",";
                }
            }

            string[] followersList = followers.Split(',');
            string[] followedList = followed.Split(',');

            foreach (string user in followedList) {
                if (Array.IndexOf(followersList, user) == -1) {
                    discrepancies += user + Environment.NewLine;
                }
            }

            fileWrite("C:\\Users\\User\\Desktop\\KaiBot\\Discrepancies.txt", discrepancies);
        }

        //private static EnumChannel GetChannel(string channel) {
        //    RestRequest request = new RestRequest("channels/{channel}", Method.GET);
        //    request.AddUrlSegment("channel", channel);
        //    IRestResponse<EnumChannel> response = restClient.Execute<EnumChannel>(request);
        //    return response.Data;
        //}

        private static EnumChannel GetChannel(string channel)
        {
            RestRequest request = new RestRequest("channels/{channel}", Method.GET);
            request.AddUrlSegment("channel", channel);
            IRestResponse<EnumChannel> response = restClient.Execute<EnumChannel>(request);
            return response.Data;
        }

        private static string GetHTML(string url) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string data = "[ERROR: Couldn't connect to (" + url + ")]";

            if (response.StatusCode == HttpStatusCode.OK) {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (response.CharacterSet == null) {
                    readStream = new StreamReader(receiveStream);
                } else {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                data = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            return data;
        }

        public static List<MessageToken> extractTokens(string msg, bool isPlainText = true) {
            char c;
            List<MessageToken> tokens = new List<MessageToken>();
            int indentation = 0;
            bool isCommand = false;
            bool isCurrentCommand = false;
            string current = "";
            bool isQuote = false;
            msg += " ";


            for (int i = 0; i < msg.Length - 1; i++) {
                c = msg[i];

                // if processing inside of command
                if (!isPlainText) {
                    if (c == '"') {
                        // if plain text is starting, save last portion and record new plain text
                        if (!isQuote) {
                            if (current != "") {
                                tokens.Add(new MessageToken(current));
                                current = "";
                            }
                        }
                        else {
                            if (current != "") {
                                List<MessageToken> t = extractTokens(current);

                                foreach (MessageToken tok in t) {
                                    tokens.Add(tok);
                                }

                                current = "";
                            }
                        }

                        isQuote = !isQuote;
                        continue;
                    }

                    if (!isQuote) {
                        if (char.IsWhiteSpace(c))
                            continue;
                    }
                }

                if (Regex.IsMatch(c + "", @"[ ,.\\\/£%^&*\[\]\{\}\-_=+<>]")) {
                    if (c == '-' && isCurrentCommand) {
                        current += c;
                        continue;
                    }

                    if (indentation == 0) {
                        isCommand = false;
                        isCurrentCommand = false;

                        if (current != "") {
                            tokens.Add(new MessageToken(current));
                            //Console.WriteLine("Tess: " + current);
                            current = "";
                        }

                        if (isPlainText || isQuote || c != ',') {
                            current = c.ToString();

                            if (Regex.IsMatch(msg[i + 1].ToString(), @"[<=>&|]")) {
                                current += msg[i + 1].ToString();
                                i++;
                            }

                            tokens.Add(new MessageToken(current));
                            current = "";
                        }
                        continue;
                    }
                }

                current += c;

                if (c == '$') {
                    isCommand = true;
                    isCurrentCommand = true;
                    continue;
                }

                if (c == '(') {
                    if (isCommand) {
                        isCurrentCommand = false;
                        indentation++;
                    }
                }

                if (c == ')') {
                    if (isCommand) {
                        isCurrentCommand = false;
                        indentation--;

                        if (indentation == 0) {
                            Console.WriteLine("current: " + current);

                            if (current.Length == 1)
                                continue;

                            string cmd = current.Substring(0, current.IndexOf('('));
                            string text = current.Substring(cmd.Length + 1, current.Length - (cmd.Length + 2));

                            MessageToken tokenCommand = new MessageToken(cmd);
                            List<MessageToken> ts = getCommandParameters(text);

                            for (int j = 0; j < ts.Count; j++) {
                                MessageToken tkns = new MessageToken("", extractTokens(ts[j].Token, false));
                                tokenCommand.Children.Add(tkns);
                            }

                            Console.WriteLine("tks: " + tokenCommand.ToString());

                            tokens.Add(tokenCommand);
                            current = "";

                        }
                    }
                }
            }

            if (!current.Equals("")) {
                tokens.Add(new MessageToken(current));
            }

            return tokens;
        }

        // Extracts the variables from the user-written command output
        public static List<MessageToken> getCommandOutputVariables(List<MessageToken> tokens) {
            List<MessageToken> output = new List<MessageToken>();

            foreach (MessageToken token in tokens) {
                if (token.Token != " " && token.Token != "," && token.Token != ".") {
                    output.Add(token);
                }
            }

            return output;
        }

        // Extracts the variables from the user message
        public static List<MessageToken> getMessageVariables(string message) {
            List<MessageToken> output = new List<MessageToken>();

            string[] messageTokens = message.Split(' ');

            foreach (string token in messageTokens) {
                if (token != "," && token != ".") {
                    output.Add(new MessageToken(token));
                }
            }

            return output;
        }

        public static List<MessageToken> getCommandParameters(string command) {
            List<MessageToken> output = new List<MessageToken>();
            List<int> indices = new List<int>();

            indices.Add(0);

            int indentation = 0;
            bool isQuote = false;
            char c;
            for (int i = 0; i < command.Length; i++) {
                c = command[i];
                if (c == '(') {
                    indentation++;
                }

                if (indentation > 0) {
                    if (c == ')') {
                        indentation--;
                    }
                }
                else if (indentation == 0) {
                    if (c == '\"') {
                        isQuote = !isQuote;
                    }

                    if (c == ',' && !isQuote) {
                        indices.Add(i);
                    }
                }
            }
            indices.Add(command.Length);

            for (int i = 1; i < indices.Count; i++) {
                string param = command.Substring(indices[i - 1], indices[i] - indices[i - 1]).Trim();
                output.Add(new MessageToken(param));
            }

            return output;
        }

        private static List<MessageToken> formatChildTokens(string sender, MessageToken token, Dictionary<string, string> dictionary) {
            List<MessageToken> childrenNew = getCommandOutputVariables(token.Children);

            for (int i = 0; i < childrenNew.Count; i++) {
                childrenNew[i] = processCommand(sender, childrenNew[i], dictionary);
            }

            token.Children = childrenNew;

            return childrenNew;
        }

        private static string getChannelInfo(string channel, string info) {
            string output;
            EnumChannel senderChannel = GetChannel(channel);

            switch (info) {
                case "name":
                    output = senderChannel.Name;
                    break;
                case "display":
                    output = senderChannel.DisplayName;
                    break;
                case "url":
                    output = senderChannel.Url;
                    break;
                case "game":
                    output = senderChannel.Game;
                    break;
                default:
                    output = null;
                    break;
            }

            return output;
        }

        public static string evaluateEquation(MessageToken token) {
            List<int> numbers = new List<int>();
            List<string> operands = new List<string>();
            if (token.Children.Count % 2 == 0) {
                return "INVALID";
            }

            string regex = @"[<=>&|]";
            for (int i = 1; i < token.Children.Count; i += 2) {
                Console.WriteLine("Op: "  + token.Children[i].Token);
                if (!Regex.IsMatch(token.Children[i].Token, regex)) {
                    return "INVALID";
                }
                operands.Add(token.Children[i].Token);
            }

            for (int i = 0; i < token.Children.Count; i += 2) {
                int value;
                Console.WriteLine("Num: " + token.Children[i].Token);

                if (!int.TryParse(token.Children[i].Token, out value)) {
                    return "INVALID";
                }

                numbers.Add(value);
            }

            for (int i = 0; i < operands.Count; i++) {
                switch(operands[i]) {
                    case "<":
                        if (!(numbers[i] < numbers[i + 1]))
                            return "False";
                        break;
                    case "<=":
                        if (!(numbers[i] <= numbers[i + 1]))
                            return "False";
                        break;
                    case ">":
                        if (!(numbers[i] > numbers[i + 1]))
                            return "False";
                        break;
                    case ">=":
                        if (!(numbers[i] >= numbers[i + 1]))
                            return "False";
                        break;
                    case "==":
                        if (!(numbers[i] == numbers[i + 1]))
                            return "False";
                        break;
                }
            }

            return "True";
        }

        public static MessageToken processCommand(string sender, MessageToken token, Dictionary<string, string> dictionary) {
            string output = token.Token;
            List<MessageToken> variables;

            //Console.WriteLine("TOKEN : " + token.Token);

            if (string.IsNullOrEmpty(token.Token)) {
                if (token.Children.Count == 0) {
                    return token;
                }
                else if (token.Children.Count == 1) {
                    return processCommand(sender, token.Children[0], dictionary);
                }
                else {
                    for (int i = 0; i < token.Children.Count; i++) {
                        output += processCommand(sender, token.Children[i], dictionary);
                    }
                    return new MessageToken(output);
                }
            }
            else if (token.Token[0] != '$') {
                return token;
            }


            string stringToken = token.Token.Substring(1);
            int indexEnd = stringToken.IndexOf('-');

            if (indexEnd > 1)
                stringToken = stringToken.Substring(0, indexEnd);

            //Console.WriteLine(stringToken);

            if (token.Token[0] == '$') {
                switch (stringToken) {
                    case "streamer":
                        if (stringToken.Length == token.Token.Length - 1) {
                            output = getChannelInfo(nameStream, "display");
                        } else {
                            string extension = token.Token.Substring(indexEnd + 2);
                            output = getChannelInfo(nameStream, extension);
                            output = (output == null) ? token.Token : output;
                        }
                        break;
                    case "bot":
                        if (stringToken.Length == token.Token.Length - 1) {
                            output = getChannelInfo(nameBot, "display");
                        } else {
                            string extension = token.Token.Substring(indexEnd + 2);
                            output = getChannelInfo(nameBot, extension);
                            output = (output == null) ? token.Token : output;
                        }
                        break;
                    case "sender":
                        if (stringToken.Length == token.Token.Length - 1) {
                            output = getChannelInfo(sender, "display");
                        } else {
                            string extension = token.Token.Substring(indexEnd + 2);
                            output = getChannelInfo(sender, extension);
                            output = (output == null) ? token.Token : output;
                        }
                        break;
                    case "time":
                        variables = formatChildTokens(sender, token, dictionary);

                        if (variables.Count == 1) {
                            DateTime time = DateTime.Now;
                            output = time.ToString(variables[0].Token);
                        } else {
                            DateTime time = DateTime.Now;
                            output = time.ToString("HH:mm:ss");
                        }
                        break;
                    case "uptime":
                        output = "AGES";
                        break;
                    case "random":
                        variables = formatChildTokens(sender, token, dictionary);

                        Console.WriteLine("random: " + token.Token);

                        for (int i = 0; i < token.Children.Count; i++) {
                            Console.WriteLine("random child: " + token.Children[i].Token);
                        }

                        if (variables.Count == 2) {
                            int[] args = new int[2];

                            if (!Int32.TryParse(variables[0].Token, out args[0])) {
                                return new MessageToken("[ERROR: Argument[0] isn't an integer]");
                            }

                            if (!Int32.TryParse(variables[1].Token, out args[1])) {
                                return new MessageToken("[ERROR: Argument[1] isn't an integer]");
                            }

                            if (args[0] >= args[1]) {
                                output = variables[0].Token;
                            }

                            Random random = new Random();
                            output = Convert.ToString(random.Next(args[0], args[1]));
                        } else {
                            output = "[ERROR: Wrong number of parameters for $random(<int1>, <int2>)]";
                        }
                        break;
                    case "apiRead":
                        variables = token.Children;

                        if (variables.Count > 0) {
                            string html = "";
                            for (int i = 0; i < variables.Count; i++) {
                                html += processCommand(sender, variables[i], dictionary).Token;
                            }

                            //Console.WriteLine(html);
                            output = GetHTML(html);
                            //output = html;
                        } else {
                            output = "[ERROR: Wrong number of parameters for $apiRead(<html>)]";
                        }
                        break;
                    case "if":
                        if (token.Children.Count == 3) {
                            MessageToken operators = new MessageToken("", formatChildTokens(sender, token.Children[0], dictionary));

                            Console.WriteLine("AFTER IF: " + operators.ToString());

                            string p0 = evaluateEquation(operators);
                            Console.WriteLine("true: " + token.Children[1].Children[0].Token);

                            if (p0 == "True")
                                output = processCommand(sender, token.Children[1], dictionary).Token;
                            else if (p0 == "False")
                                output = processCommand(sender, token.Children[2], dictionary).Token;
                            else
                                output = "[ERROR: Expecting \"True\" or \"False\"]";
                        }

                        break;
                    default:
                        if (Regex.IsMatch(token.Token.Trim(), @"(^\$user\d*(-\w+)*)$")) {
                            if (dictionary.ContainsKey(token.Token.Substring(0,5))) {
                                string[] parts = token.Token.Split('-');
                                if (stringToken.Length == token.Token.Length - 1) {
                                    output = token.Token;
                                } else {
                                    string extension = token.Token.Substring(indexEnd + 2);
                                    output = getChannelInfo(dictionary[parts[0]], extension);
                                    output = (output == null) ? token.Token : output;
                                }
                            }
                        }
                        else if (dictionary.ContainsKey(token.Token)) {
                            output = dictionary[token.Token];
                        }

                        break;
                }
            }

            return new MessageToken(output);
        }

        private static void printTokenTree(MessageToken token, string old) {
            Console.WriteLine(old + " " + token.Token + "<" + token.Children.Count + ">");

            for (int i = 0; i < token.Children.Count; i++) {
                printTokenTree(token.Children[i], old + "[" + token.Token + "]");
            }
        }

        private static string tokensToString(string sender, List<MessageToken> tokens, Dictionary<string, string> dictionary) {
            string current = "";

            for (int j = 0; j < tokens.Count; j++) {
                printTokenTree(tokens[j], "[S]");
            }

            for (int i = 0; i < tokens.Count; i++) {
                current += processCommand(sender, tokens[i], dictionary).Token;
            }

            return current;
        }

        private static Message getMessage(string sender, string body) {
            string message = "";
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            for (int i = 0; i < commands.Count; i++) {
                if (commands[i].isMatch(body)) {
                    // get variables values from message
                    List<MessageToken> tokensMessage = extractTokens(body);

                    tokensMessage = getCommandOutputVariables(tokensMessage);

                    // Assign values to variables in command 
                    dictionary = Command.getVariableDictionary(commands[i].getCallerVariables(), tokensMessage);

                    string response = commands[i].Response;

                    // Replaces variables with values in command response
                    foreach (KeyValuePair<string, string> entry in dictionary) {
                        //response = response.Replace(entry.Key, entry.Value);
                    }

                    List<MessageToken> tokens = extractTokens(response);

                    message = tokensToString(sender, tokens, dictionary);

                }
            }

            return new Message(sender, message);
        }

        private void Disconnect() {
            connect = false;
            thread_tick.Join();
        }
		
		private void Window_Loaded(object sender, RoutedEventArgs e) {
			EnableBlur();
        }

        private void Window_Close(object sender, RoutedEventArgs e) {
            Disconnect();
            Close();
        }

        private void Window_Minimize(object sender, RoutedEventArgs e) {
            WindowState = WindowState.Minimized;
        }

        private void Window_Maximize(object sender, RoutedEventArgs e) {
            Close();
        }



        internal void EnableBlur() {
			var windowHelper = new WindowInteropHelper(this);
			
			var accent = new AccentPolicy();
			accent.AccentState = AccentState.ACCENT_ENABLE_BLURBEHIND;

			var accentStructSize = Marshal.SizeOf(accent);

			var accentPtr = Marshal.AllocHGlobal(accentStructSize);
			Marshal.StructureToPtr(accent, accentPtr, false);

			var data = new WindowCompositionAttributeData();
			data.Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY;
			data.SizeOfData = accentStructSize;
			data.Data = accentPtr;
			
			SetWindowCompositionAttribute(windowHelper.Handle, ref data);

			Marshal.FreeHGlobal(accentPtr);
		}

        private void TitleBar_MouseDown_Left(object sender, MouseButtonEventArgs e) {
            DragMove();
        }
    }
}
