﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace KaiBot {

    struct UserDisplay {
        public string UserName { get; set; }
        public Color UserColour { get; set; }
    }

    class Session {
        List<UserDisplay> listUser;
        Color[] listColour = { Colors.Red, Colors.Blue, Colors.Green, Colors.Yellow, Colors.Pink, Colors.Purple, Colors.Orange };

        public Session() {

        }

        public void Start() {
            listUser = new List<UserDisplay>();
        }

        public MessageFormatted getFormattedMessage(string sender, string body) {
            Color colour = Colors.Red;
            bool found = false;

            foreach (UserDisplay u in listUser) {
                if (u.UserName.Equals(sender)) {
                    colour = u.UserColour;
                    found = true;
                    break;
                }
            }

            if (!found) {
                Random random = new Random();
                colour = listColour[random.Next(0, listColour.Length)];
                listUser.Add(new UserDisplay { UserName = sender, UserColour = colour });
            }

            MessageFormatted message = new MessageFormatted();
            message.Sender = sender;
            message.Body = body;
            message.NameColour = new SolidColorBrush(colour);

            return message;
        }
    }
}
