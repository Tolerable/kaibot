﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KaiBot {
    public class CounterDataViewModel {
    public ObservableCollection<CounterData> Data { get; set; }

    public XAMLCommand AddNewRowCommand { get; set; }

    public XAMLCommand<CounterData> Command1 { get; set; }

    public XAMLCommand<CounterData> Command2 { get; set; }

    public CounterDataViewModel()
    {
        var sampledata = Enumerable.Range(0, 1).Select(x => new CounterData()
                                                {
                                                    Label1Text = "Label1 " + x.ToString(),
                                                    Text = "Text" + x.ToString()
                                                });

        Data = new ObservableCollection<CounterData>(sampledata);
        AddNewRowCommand = new XAMLCommand(AddNewRow);
        Command1 = new XAMLCommand<CounterData>(ExecuteCommand1);
        Command2 = new XAMLCommand<CounterData>(ExecuteCommand2);

    }

    private void AddNewRow()
    {
        Data.Add(new CounterData() {Label1Text = "Label 1 - New Row", Text = "New Row Text"});
    }

    private void ExecuteCommand1(CounterData data)
    {
        MessageBox.Show("Command1 - " + data.Label1Text);
    }

    private void ExecuteCommand2(CounterData data)
    {
        MessageBox.Show("Command2 - " + data.Label1Text);
    }
    }
}
