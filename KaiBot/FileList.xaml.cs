﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;

namespace KaiBot {
    
	public partial class FileList : Window {
		[DllImport("user32.dll")]
		internal static extern int SetWindowCompositionAttribute(IntPtr hwnd, ref WindowCompositionAttributeData data);
        int desktop_width = 1920;
        int desktop_height = 1080;
        int selected_id = 0;

        public FileList(string path) {
			InitializeComponent();
            Height = 750;
            Width = 450;
            Left = desktop_width - Width - 150;
            Top = desktop_height - Height - 100;

            string[] file_list = Directory.GetFiles(path);

            int nums = 0;

            for (int i = 0; i < file_list.Length; i++) {
                string content = file_list[i].Substring(file_list[i].Length - 4);
                if (!content.Equals(".txt")) {
                    continue;
                }

                content = file_list[i].Remove(file_list[i].Length - 4);
                content = content.Remove(0, path.Length);

                Thickness margin = new Thickness();
                margin.Top = nums * 25;

                Thickness border = new Thickness(0);

                Button b = new Button();
                b.Content = content;
                b.VerticalAlignment = VerticalAlignment.Top;
                b.Background = Brushes.Transparent;
                b.Height = 25;
                b.Margin = margin;
                b.BorderThickness = border;
                b.IsCancel = true;
                
                grid_docked.Children.Add(b);
                nums++;
            }

        }

        public static bool Get_File(string path) {
            FileList list = new FileList(path);
            list.ShowDialog();



            bool ok = true;

            return ok;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
			EnableBlur();
        }

        internal void EnableBlur() {
			var windowHelper = new WindowInteropHelper(this);
			
			var accent = new AccentPolicy();
			accent.AccentState = AccentState.ACCENT_ENABLE_BLURBEHIND;

			var accentStructSize = Marshal.SizeOf(accent);

			var accentPtr = Marshal.AllocHGlobal(accentStructSize);
			Marshal.StructureToPtr(accent, accentPtr, false);

			var data = new WindowCompositionAttributeData();
			data.Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY;
			data.SizeOfData = accentStructSize;
			data.Data = accentPtr;
			
			SetWindowCompositionAttribute(windowHelper.Handle, ref data);

			Marshal.FreeHGlobal(accentPtr);
        }
    }
}
