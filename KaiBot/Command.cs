﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaiBot {
    public class Command {
        public string Call { get; set; }
        public string Response { get; set; }
        public List<Command> Extensions { get; set; }

        public List<MessageToken> getCallerVariables() {
            List<MessageToken> tokens = new List<MessageToken>();
            string[] header = Call.Split(' ');

            foreach (string token in header) {
                tokens.Add(new MessageToken(token));
            }

            return tokens;
        }

        public bool isMatch(string message) {
            string[] tokensMessage = message.Split(' ');
            List<MessageToken> tokensCaller = getCallerVariables();

            if (tokensMessage.Length == tokensCaller.Count) {
                if (tokensMessage[0].Trim() == tokensCaller[0].Token.Trim()) {
                    return true;
                }
            }

            return false;
        }

        public List<MessageToken> getTokens() {
            List<MessageToken> tokens = Main.extractTokens(Response);

            return tokens;
        }

        public static Dictionary<string, string> getVariableDictionary(List<MessageToken> tokensCommand, List<MessageToken> tokensMessage) {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            for (int i = 0; i < tokensCommand.Count; i++) {
                if (tokensCommand[i].Token.StartsWith("$")) {

                    dictionary.Add(tokensCommand[i].Token, tokensMessage[i].Token);
                }
            }

            return dictionary;
        }
    }

    public class ExtensionSender : Command {

    }
}
